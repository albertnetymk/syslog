exports = this
express = require('express')

exports.num_error_msg = num_error_msg = 'num is not a valid number'
exports.startTime_error_msg = startTime_error_msg = 'startTime is not a valid date'
exports.endTime_error_msg = endTime_error_msg = 'endTime is not a valid date'

parse_num = (num_str) ->
  status = true
  num = parseInt(num_str, 10)

  if isNaN num
    status = false
  else if num <= 0
    error_msg = '+ve'
    status = false
  {status: status , num: num}

parse_date = (date_string) ->
  status = true
  date = Date.parse(date_string)

  if isNaN date
    status = false
  else
    date = new Date date_string
  {status: status , date: date}

db = null

exports.set_db = (_db) ->
  db = _db

exports.start = start = (port, cb) ->
  rest = express()
  rest.get '/stats', (req, res) ->
    db.stats (obj) ->
     res.send JSON.stringify(obj) + "\n"

  rest.get '/logs', (req, res) ->
    error_msgs = []

    num_str = req.query.num || '20'
    {status, num} = parse_num num_str
    error_msgs.push num_error_msg if not status

    startTime_str = req.query.startTime
    if startTime_str
      {status, date} = parse_date startTime_str
      startTime = date
      error_msgs.push startTime_error_msg if not status
    else
      startTime = undefined

    endTime_str = req.query.endTime
    if endTime_str
      {status, date} = parse_date endTime_str
      endTime = date
      error_msgs.push endTime_error_msg if not status
    else
      endTime = undefined

    ip = req.query.ip
    cat = req.query.cat
    event = req.query.event
    action = req.query.action
    if error_msgs.length > 0
      res.status 400
      res.send error_msgs.join('\n') + '\n'
      return
    db.logs num, startTime, endTime, ip, cat, event, action, (obj) ->
      res.json obj

  cb rest.listen port

exports.stop = (server) ->
  server.close()
