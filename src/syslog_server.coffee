exports = this
dgram = require('dgram')

parser = require './parser'
p = console.log

db = null

exports.set_db = set_db = (_db) ->
  db = _db

exports.start = (syslog_port, cb) ->
  syslog = dgram.createSocket('udp4')
  syslog.on 'message', (message, remote) ->
    msg_obj = parser.parse_msg message.toString()
    msg_obj.ip = remote.address
    db.create msg_obj
    return
  syslog.bind syslog_port, ->
    cb syslog

exports.stop = (syslog) ->
  syslog.close()
