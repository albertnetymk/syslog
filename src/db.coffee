exports = this
nedb = require('nedb')
async = require 'async'
p = console.log

db = null

print_db = ->
  db.find {}, (err, docs) ->
    for doc in docs
      p doc

exports.init_db = init_db = (in_mem = true)->
  if in_mem
    db = new nedb
  else
    db = new nedb({filename: 'db/logdb.json', autoload: true})

exports.docs = docs = (cb) ->
  db.find {}, (err, docs) ->
    cb docs

exports.create = create = (msg_obj, cb) ->
  if msg_obj.status
    db.insert msg_obj
    db.update({ _id: 'received_id', name: 'received' }, { $inc: { counter : 1 } }, { upsert: true})
  else
    db.update({ _id: 'malformed_id', name: 'malformed' }, { $inc: { counter : 1 } }, { upsert: true})
  cb?()

exports.stats = stats = (cb) ->
  async.parallel {
    received: (cb) ->
      db.findOne { name: 'received' }, (err, doc) ->
        if doc
          cb null, doc.counter
        else
          cb null, 0
    malformed: (cb) ->
      db.findOne { name: 'malformed' }, (err, doc) ->
        if doc
          cb null, doc.counter
        else
          cb null, 0
    first: (cb) ->
      db.find({ status: true }).sort({event_date:1}).limit(1).exec (err, docs) ->
        if docs.length is 0
          cb null, null
        else
          cb null, docs[0].event_date
    last: (cb) ->
      db.find({ status: true }).sort({event_date:-1}).limit(1).exec (err, docs) ->
        if docs.length is 0
          cb null, null
        else
          cb null, docs[0].event_date
  }, (err, results) ->
    cb results

exports.logs = logs = (num, startTime, endTime, ip, cat, event, action, cb) ->
  query = { status: true }
  range = {}
  range['$gt'] = startTime if startTime
  range['$lt'] = endTime if endTime

  query.event_date = range if startTime or endTime
  query.ip = ip if ip
  query.category = { $regex: new RegExp(cat, 'i') } if cat
  query.event = { $regex: new RegExp(event, 'i') } if event
  query.action = { $regex: new RegExp(action, 'i') } if action

  db.find(query).sort({event_date:-1}).limit(num).exec (err, docs) ->
    logs = []
    for doc in docs
      obj =
        time: doc.event_date
        ip:doc.ip
        cat: doc.category
        event: doc.event
        action: doc.action
        message: doc.message
      logs.push obj
    cb logs
