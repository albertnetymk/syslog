exports = this
p = console.log

parsec = require 'parsimmon'

space = parsec.optWhitespace;
string = parsec.string;
digit = parsec.digit;
digits = parsec.digits;
letter = parsec.letter;
letters = parsec.letters;
all = parsec.all

seq = (parser_list) ->
  r = parsec.seq.apply this, parser_list
  r.map (x) -> x.join('')

pri_parser =
  do ->
    format = [string('<'), digits, string('>')]
    seq format

event_date_parser =
  do ->
    dash = string('-')
    year = month = day = hour = minute = second = digits
    colon = string(':')
    format = [
      string('['),
      year, dash, month, dash, day, string(' '),
      hour, colon, minute, colon, second,
      string(']')
    ]
    seq format

efw_parser =
  do ->
    format = [string("EFW:")]
    seq format

category_parser =
  do ->
    format = [letters, string(':')]
    seq format

prio_parser =
  do ->
    format = [string("prio=").then(digit)]
    seq format

id_parser =
  do ->
    format = [string("id=").then(digits)]
    seq format

rev_parser =
  do ->
    format = [string("rev=").then(digit)]
    seq format

event_parser =
  do ->
    format = [string("event=").then(letter.or(string('_')).many().map((e) -> e.join('')))]
    seq format

action_parser =
  do ->
    format = [string("action=").then(letter.or(string('_')).many().map((e) -> e.join('')))]
    seq format

message_parser =
  all.map((e) -> if e is '' then null else e)

exports.parse_msg = parse_msg = (msg) ->
  format = [
    pri_parser
    event_date_parser
    space.result ''
    efw_parser
    space.result ''
    category_parser
    space.result ''
    prio_parser
    space.result ''
    id_parser
    space.result ''
    rev_parser
    space.result ''
    event_parser
    space.result ''
    action_parser.or(space.result(null))
    space.result ''
    message_parser
  ]
  parser = parsec.seq.apply this, format
  {status, value, index, expected} = err = parser.parse msg
  if status
    value = value.filter((e) -> e != '')
    [pri, event_date, efw, category, prio, id, rev, event, action, message] = value
    obj =
      status: status
      pri: pri
      event_date: new Date event_date[1..event_date.length-2]
      efw: efw[..efw.length-2]
      category: category[..category.length-2]
      prio: prio
      id: id
      rev: rev
      event: event
      action: action || undefined
      message: message || undefined
  else
    obj =
      status: status
      msg: parsec.formatError(msg, err)
