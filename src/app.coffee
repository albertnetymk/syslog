express = require('express')
argv = require('minimist')(process.argv.slice(2))

syslog_server = require('./syslog_server.coffee')
rest_api_server = require('./rest_api_server.coffee')

db = require './db'

p = console.log

db.init_db(false)

syslog_port = argv['s'] || 3000
rest_port = argv['r'] || 3001

syslog_server.set_db(db)
syslog_server.start syslog_port, ->
  console.log "syslog server started on port #{syslog_port}"

rest_api_server.set_db(db)
rest_api_server.start rest_port, ->
  console.log 'rest api server started on port #{rest_port}'
