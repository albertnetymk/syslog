"use strict"
p = console.log

async = require 'async'
http = require 'http'

rest_api_server = APP.rest_api_server

describe 'rest api server', ->
  port = 30001
  it 'starts and closes rest api server', (done) ->
    async.waterfall [
      (cb) -> rest_api_server.start port, (server) -> cb null, server
      (server, cb) -> rest_api_server.stop server; cb()
      (cb) -> rest_api_server.start port, (server) -> cb null, server
      (server, cb) ->
        rest_api_server.stop server
        done()
    ]

  describe '/logs url params validation', (done) ->
    port = port
    server = null
    before (done) ->
      rest_api_server.start port, (_server) -> server = _server; done()

    it 'returns NaN when num is passed with non-number', (done) ->
      url =
        port: port
        path: '/logs?num=r'

      http.get url, (res) ->
        eq res.statusCode, 400
        body = ''
        res.on 'data', (d) ->
          body += d
        res.on 'end', (d) ->
          eq body, rest_api_server.num_error_msg + '\n'
          done()

    it 'returns NaD when startTime is passed with non-date', (done) ->
      url =
        port: port
        path: '/logs?startTime=r'

      http.get url, (res) ->
        eq res.statusCode, 400
        body = ''
        res.on 'data', (d) ->
          body += d
        res.on 'end', (d) ->
          eq body, rest_api_server.startTime_error_msg + '\n'
          done()

    it 'returns NaD when endTime is passed with non-date', (done) ->
      url =
        port: port
        path: '/logs?endTime=r'

      http.get url, (res) ->
        eq res.statusCode, 400
        body = ''
        res.on 'data', (d) ->
          body += d
        res.on 'end', (d) ->
          eq body, rest_api_server.endTime_error_msg + '\n'
          done()

    after (done) ->
      rest_api_server.stop server
      done()
