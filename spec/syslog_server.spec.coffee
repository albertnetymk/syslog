"use strict"

p = console.log

dgram = require 'dgram'
async = require 'async'

db = APP.db
syslog_server = APP.syslog_server

describe 'syslog server', ->
  port = 30000
  it 'starts and closes syslog server', (done) ->
    async.waterfall [
      (cb) -> syslog_server.start port, (server) -> cb null, server
      (server, cb) -> syslog_server.stop server; cb()
      (cb) -> syslog_server.start port, (server) -> cb null, server
      (server, cb) ->
        syslog_server.stop server
        done()
    ]

  describe 'send msg to syslog server', ->
    client = dgram.createSocket('udp4')
    server = null

    beforeEach (done) ->
      db.init_db()
      syslog_server.set_db db
      syslog_server.start port, (_server) ->
        server = _server
        done()

    it 'saves msg into db', (done) ->
      msg = new Buffer '<123>[2014-09-01 13:04:11] EFW: SYSTEM: prio=2 id=03204001 rev=1 event=accept_configuration action=using_new_config'
      client.send msg, 0, msg.length, port, 'localhost'
      setTimeout( ->
        db.docs (docs) ->
          eq docs.length, 2
          done()
      , 10)

    it 'rejects invalid msg into db', (done) ->
      msg = new Buffer '<123[2014-09-01 13:04:11] EFW: SYSTEM: prio=2 id=03204001 rev=1 event=accept_configuration action=using_new_config'
      client.send msg, 0, msg.length, port, 'localhost'
      setTimeout( ->
        db.docs (docs) ->
          eq docs.length, 1
          done()
      , 10)

    afterEach (done) ->
      syslog_server.stop server
      done()
