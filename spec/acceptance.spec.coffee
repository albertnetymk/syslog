"use strict"

p = console.log

http = require 'http'
dgram = require 'dgram'
async = require 'async'
request = require 'request'

db = APP.db
syslog_server = APP.syslog_server
rest_api_server = APP.rest_api_server

msgs = [
  '<123>[2010-09-01 13:04:11] EFW: SYSTEM: prio=2 id=03204001 rev=1 event=accept_configuration action=using_new_config'
  '<134>[2011-09-01 13:04:04] EFW: NETCON: prio=1 id=02300001 rev=1 event=init_complete'
  '<134>[2012-09-01 13:04:09] EFW: SYSTEM: prio=2 id=03202001 rev=2 event=startup_echo delay=5 corever=10.21.00.15-24791 build="Aug 20 2014" uptime=6243 cfgfile="core.cfg" localcfgver=258 remotecfgver=0 previous_shutdown="2014-09-01 13:03:59: Activating configuration changes"'
  '<134>[2013-09-01 13:04:11] EFW: SYSTEM: prio=2 id=03204001 rev=1 event=accept_configuration action=using_new_config username=admin userdb=AdminUsers client_ip=172.16.10.1 config_system=HTTP'
  '<134[2014-09-01 13:04:11] EFW: SYSTEM: prio=2 id=03204001 rev=1 event=accept_configuration action=using_new_config username=admin userdb=AdminUsers client_ip=172.16.10.1 config_system=HTTP'
]

describe 'acceptance test', ->
  syslog_port = 30000
  rest_port = 30001
  client = dgram.createSocket('udp4')

  before (done) ->
    db.init_db()
    syslog_server.set_db db
    rest_api_server.set_db db
    async.series [
      (cb) -> syslog_server.start syslog_port, -> cb null, null
      (cb) -> rest_api_server.start rest_port, -> cb null, null
      (cb) ->
        async.each(msgs, (m, _cb) ->
          msg = new Buffer m
          client.send msg, 0, msg.length, syslog_port, 'localhost'
          _cb()
        , (err) -> setTimeout done, 10
        )
    ]

  it 'returns right stats', (done) ->
    url = "http://localhost:#{rest_port}/stats"
    request
      url: url
      json: true
      (err, res, body) ->
        eq body.received, 4
        eq body.malformed, 1
        eq body.first, new Date('2010-09-01 13:04:11').toISOString()
        eq body.last, new Date('2013-09-01 13:04:11').toISOString()
        done()

  it 'returns num logs', (done) ->
    async.parallel
      one: (cb) ->
        url = "http://localhost:#{rest_port}/logs?num=1"
        request
          url: url
          json: true
          (err, res, body) ->
            cb null, body.length
      two: (cb) ->
        url = "http://localhost:#{rest_port}/logs?num=2"
        request
          url: url
          json: true
          (err, res, body) ->
            cb null, body.length
    , (err, result) ->
      eq result.one, 1
      eq result.two, 2
      done()

  it 'returns logs btw startTime and endTime', (done) ->
    startTime = '2011-09-01T13:04:04Z'
    endTime = '2012-09-01T13:04:09Z'
    url = "http://localhost:#{rest_port}/logs?startTime=#{startTime}&endTime=#{endTime}"
    request
      url: url
      json: true
      (err, res, body) ->
        eq body.length, 1
        done()
