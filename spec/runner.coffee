"use strict"

Mocha = require('mocha')
should = require 'should'

eq = (a, b) ->
  if a
    a.should.equal b
  else if a is null
    (b is null).should.equal true
  else
    (b is undefined).should.equal true

GLOBAL.eq = eq

GLOBAL.APP = {}
GLOBAL.APP.parser = require('../src/parser');
GLOBAL.APP.db = require('../src/db');
GLOBAL.APP.rest_api_server = require('../src/rest_api_server');
GLOBAL.APP.syslog_server = require('../src/syslog_server');

opts =
  ui : 'bdd'
  reporter : (process.env.REPORTER || 'spec')
  grep : process.env.GREP
  compilers : 'coffee:coffee-script'
  require: 'should'

opts.reporter = 'dot' if process.env.TRAVIS;

m = new Mocha(opts)

m.invert() if process.env.INVERT

m.addFile('spec/parser.spec.coffee');
m.addFile('spec/db.spec.coffee');
m.addFile('spec/rest_api_server.spec.coffee');
m.addFile('spec/syslog_server.spec.coffee');
m.addFile('spec/acceptance.spec.coffee');

m.run (err) ->
    exitCode = err? 1 : 0
    process.exit(exitCode)
