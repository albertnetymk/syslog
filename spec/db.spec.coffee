"use strict"

async = require 'async'

db = APP.db

p = console.log

describe 'database', ->
  beforeEach ->
    db.init_db true

  it 'starts with empty db', ->
    db.docs (docs) ->
      docs.length.should.equal 0

  it 'counts the number of successfully logged message', (done) ->
    msg_obj =
      status: true
      pri: '<123>'
      event_date: new Date '2014-09-01 13:04:11'
      efw: 'EFW'
      category: 'SYSTEM'
      prio: '2'
      id: '03204001'
      rev: '1'
      event: 'accept_configuration'
    async.series [
      (cb) -> db.create msg_obj, ->
        cb null, null
      (cb) -> db.docs (docs) ->
        docs.length.should.equal 2
        counter_obj = d for d in docs when d.status is undefined
        counter_obj.counter.should.equal 1
        done()
    ]

  it 'counts the number of malformed logged message', (done) ->
    msg_obj =
      status: false
    async.series [
      (cb) -> db.create msg_obj, ->
        cb null, null
      (cb) -> db.docs (docs) ->
        docs.length.should.equal 1
        counter_obj = d for d in docs when d.status is undefined
        counter_obj.name.should.equal 'malformed'
        counter_obj.counter.should.equal 1
        done()
    ]
  it 'provides stats info', (done) ->
    good_msg_obj =
      status: true
      pri: '<123>'
      event_date: new Date '2014-09-01 13:04:11'
      efw: 'EFW'
      category: 'SYSTEM'
      prio: '2'
      id: '03204001'
      rev: '1'
      event: 'accept_configuration'
    bad_msg_obj =
      status: false
    async.series [
      (cb) -> db.create good_msg_obj, ->
        cb null, null
      (cb) -> db.create bad_msg_obj, ->
        cb null, null
      (cb) -> db.stats (info) ->
        info.received.should.equal 1
        info.malformed.should.equal 1
        info.first.toString().should.equal new Date('2014-09-01 13:04:11').toString()
        info.last.toString().should.equal new Date('2014-09-01 13:04:11').toString()
        done()
    ]
  describe 'logs query', ->
    beforeEach (done) ->
      async.series [
        (cb) ->
          msg_obj =
            status: true
            pri: '<123>'
            event_date: new Date '2013-09-01 13:04:11'
            efw: 'EFW'
            category: 'FIRST'
            prio: '2'
            id: '03204001'
            rev: '1'
            event: 'accept_configuration'
          db.create msg_obj, ->
            cb null, null
        (cb) ->
          msg_obj =
            status: true
            pri: '<123>'
            event_date: new Date '2014-09-01 13:04:11'
            efw: 'EFW'
            category: 'SYSTEM'
            prio: '2'
            id: '03204001'
            rev: '1'
            event: 'accept_configuration'
          db.create msg_obj, ->
            cb null, null
        (cb) ->
          msg_obj =
            status: true
            pri: '<123>'
            event_date: new Date '2015-09-01 13:04:11'
            efw: 'EFW'
            category: 'THIRD'
            prio: '2'
            id: '03204001'
            rev: '1'
            event: 'accept_configuration'
          db.create msg_obj, ->
            done()
      ]
    it 'returns all logs when num is nul', (done) ->
      db.logs null, null, null, null, null, null, null, (logs) ->
        logs.length.should.equal 3
        done()

    it 'returns all logs after startTime', (done) ->
      db.logs null, new Date('2013-09-01 13:04:11'), null, null, null, null, null, (logs) ->
        logs.length.should.equal 2
        done()

    it 'returns all logs before endTime', (done) ->
      db.logs null, null, new Date('2015-09-01 13:04:11'), null, null, null, null, (logs) ->
        logs.length.should.equal 2
        done()

    it 'returns all logs btw startTime and endTime', (done) ->
      startTime = new Date('2013-09-01 13:04:11')
      endTime = new Date('2015-09-01 13:04:11')
      db.logs null, startTime, endTime, null, null, null, null, (logs) ->
        logs.length.should.equal 1
        done()
