Syslog server with REST API.

This repo mainly focuses on how one would create a small project, using some
good practice, so it's more for pedagogical purpose than solving any real world
problem.

* Uses parser combinator (provided by
  [parsimmon](https://www.npmjs.com/package/parsimmon)).

* Uses [nedb](https://github.com/louischatriot/nedb) for storage.

* Uses [mocha](http://mochajs.org) for testing.
